package fi.vamk.e1801917.ass;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.context.annotation.Bean;

@SpringBootApplication
public class Application {

	@Autowired
	private AttendanceRepository repository;

	public static void main(String[] args) {
		SpringApplication.run(Application.class, args);
	}

	@Bean
	public void init(){
		Attendance_Input att = new Attendance_Input("1","abcd","12-12-2000");
		Attendance_Input att1 = new Attendance_Input("2","abcd","12-12-2000");

		repository.save(att.convert());
		repository.save(att1.convert());
	}


}
