package fi.vamk.e1801917.ass;

import java.text.ParseException;
import java.text.SimpleDateFormat;

public class Attendance_Input {
	
	private String id;
	private String key;
	private String day;
	
	public Attendance_Input(String id, String key, String day) {
		super();
		this.id = id;
		this.key = key;
		this.day = day;
	}
	public String getDay() {
		return this.day;
	}
	public void setDay(String day) {
		this.day = day;
	}
	public String getId() {
		return this.id;
	}
	public void setId(String id) {
		this.id = id;
	}

	public String getKey() {
		return this.key;
	}

	public void setKey(String key) {
		this.key = key;
	}
	
	
	public Attendance convert()
	{
		try {
			return new Attendance(Integer.parseInt( this.getId()), this.getKey(), new SimpleDateFormat("dd-MM-yyyy").parse(this.getDay()));
		} catch (ParseException e) {
			return new Attendance(Integer.parseInt( this.getId()), this.getKey(), null);
		}		
	}
	
	
	
	
	public String toString() {
		return id + "-" + key + "-" + day;
	}

}
